import mss
import numpy

from line_detector import detect_fishing_line

with mss.mss() as sct:
    monitor = {'left': 560, 'top': 240, 'width': 800, 'height': 640}

    while True:
        # this is the intended way of using mss
        # noinspection PyTypeChecker
        image = numpy.array(sct.grab(monitor))
        detect_fishing_line('main monitor', image)
