import os
import cv2
import numpy as np

sobel_1 = np.array([[0, 1, 2],
                    [-1, 0, 1],
                    [-2, -1, 0]])
sobel_2 = np.array([[-2, -1, 0],
                    [-1, 0, 1],
                    [0, 1, 2]])

dilation_1 = np.array([[0, 0, 0, 0, 0],
                       [0, 1, 1, 0, 0],
                       [0, 1, 1, 1, 0],
                       [0, 0, 1, 1, 0],
                       [0, 0, 0, 0, 0]], dtype=np.uint8)
dilation_2 = np.array([[0, 0, 0, 0, 0],
                       [0, 0, 1, 1, 0],
                       [0, 1, 1, 1, 0],
                       [0, 1, 1, 0, 0],
                       [0, 0, 0, 0, 0]], dtype=np.uint8)


def detect_fishing_line(window_name, image):
    blurred = cv2.blur(image, (3, 3))
    gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)

    filtered_1 = cv2.filter2D(gray, -1, sobel_1)
    filtered_2 = cv2.filter2D(gray, -1, sobel_2)

    ret, mask_1 = cv2.threshold(filtered_1, 96, 255, cv2.THRESH_BINARY)
    ret, mask_2 = cv2.threshold(filtered_2, 96, 255, cv2.THRESH_BINARY)

    dilated_1 = cv2.dilate(mask_1, dilation_1)
    dilated_2 = cv2.dilate(mask_2, dilation_2)

    contours_1, hierarchy = cv2.findContours(dilated_1, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    contours_2, hierarchy = cv2.findContours(dilated_2, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    contours = contours_1 + contours_2

    if len(contours) > 0:
        result = np.zeros(dilated_1.shape)

        max_cnt = max(contours, key=lambda cnt: cv2.boundingRect(cnt)[2])  # biggest width
        cv2.drawContours(image, [max_cnt], 0, (0, 255, 0), 3)
        cv2.drawContours(result, [max_cnt], 0, (255, 255, 255), 3)

        x, y, w, h = cv2.boundingRect(max_cnt)
        roi_size = 48
        cv2.rectangle(image, (x, y), (x + roi_size, y + roi_size), (255, 0, 0), 2)

        result = result[y:y + roi_size, x:x + roi_size]

        cv2.imshow(window_name, image)
        cv2.imshow(window_name + ' result', cv2.pyrUp(cv2.pyrUp(result)))
        cv2.waitKey(1)

        return result

    return None


def main():
    images = []
    image_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'samples')
    for filename in os.listdir(image_directory):
        if filename.endswith('.png'):
            images.append(filename)

    for image_name in images:
        image = cv2.imread(os.path.join(image_directory, image_name))
        detect_fishing_line(image_name, image)

    cv2.waitKey(0)


if __name__ == '__main__':
    main()
